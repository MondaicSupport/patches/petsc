import inspect
import pathlib

# Current path.
root_path = pathlib.Path(inspect.getfile(inspect.currentframe())).parent

cmake_file = root_path / "CMakeLists.txt"


project_name = "PETSC_MSVC"

ignore = [
    # dm
    "src/dm/impls/da/usfft",
    "src/dm/impls/da/moab",
    "src/dm/impls/da/hypre",
    "src/dm/impls/da/kokkos",
    "src/dm/impls/da/usfft",
    "src/dm/impls/forest/p4est",
    "src/dm/impls/plex/adaptors",
    "src/dm/impls/plex/generators",
    "src/dm/impls/plex/cgns",
    # sys
    "src/sys/ams",
    "src/sys/classes/draw/impls/x",
    "src/sys/classes/matlabengine",
    "src/sys/classes/random/impls/curand",
    "src/sys/classes/random/impls/random123",
    "src/sys/classes/random/impls/sprng",
    "src/sys/logging/handler/impls/mpe",
    "src/sys/logging/handler/impls/nvtx",
    "src/sys/mpiuni",
    "src/sys/webclient",
    "src/sys/yaml",
    "src/sys/classes/viewer/impls/adios",
    "src/sys/classes/viewer/impls/ams",
    "src/sys/classes/viewer/impls/cgns",
    "src/sys/classes/viewer/impls/mathematica",
    "src/sys/classes/viewer/impls/matlab",
    "src/sys/classes/viewer/impls/socket",
    # "src/sys/classes/viewer/impls/hdf5",
    # mat
    "src/mat/impls/aij/mpi/mkl_cpardiso",
    "src/mat/impls/aij/mpi/mumps",
    "src/mat/impls/aij/mpi/pastix",
    "src/mat/impls/aij/mpi/strumpack",
    "src/mat/impls/aij/mpi/superlu_dist",
    "src/mat/impls/aij/seq/aijmkl",
    "src/mat/impls/aij/seq/cholmod",
    "src/mat/impls/aij/seq/klu",
    "src/mat/impls/aij/seq/matlab",
    "src/mat/impls/aij/seq/mkl_pardiso",
    "src/mat/impls/aij/seq/spqr",
    "src/mat/impls/aij/seq/superlu",
    "src/mat/impls/aij/seq/umfpack",
    "src/mat/impls/baij/seq/baijmkl",
    "src/mat/impls/sbaij/seq/cholmod",
    "src/mat/impls/fft/fftw",
    "src/mat/impls/hypre",
    "src/mat/partition/impls/party",
    "src/mat/partition/impls/pmetis",
    "src/mat/order/amd",
    "src/mat/order/metisnd",
    # ksp
    "src/ksp/ksp/interface/saws",
    "src/ksp/pc/impls/h2opus",
    "src/ksp/pc/impls/hypre",
    "src/ksp/pc/impls/ml",
    "src/ksp/pc/impls/parms",
    "src/ksp/pc/impls/spai",
    # snes
    "src/snes/interface/saws",
    # ts
    "src/ts/impls/implicit/radau5",
    "src/ts/impls/implicit/sundials",
    "src/ts/impls/visualization",
    # vec
    "src/vec/vec/impls/hypre",
    "src/vec/vec/utils/matlab",
]


def filter_filename(filename: pathlib.Path) -> bool:
    if not filename.is_file():
        return False

    filename = pathlib.PurePosixPath(filename.relative_to(root_path))
    # Skip benchmarks and extra bindings.
    if filename.parts[1] in ["benchmarks", "binding"]:
        return False
    elif "tutorials" in filename.parts or "tests" in filename.parts:
        return False
    # Ignore the Fortran interfaces.
    elif "f90-custom" in filename.parts or "ftn-custom" in filename.parts:
        return False
    elif "visualization" in filename.parts:
        return False
    # We don't need the Python bindings, but we do need the shims in the
    # pythonsys.c file.
    elif "python" in filename.parts and filename.name != "pythonsys.c":
        return False

    while str(filename) != ".":
        if str(filename) in ignore:
            return False
        filename = filename.parent

    return True

def find_c_files(root: pathlib.Path):
    return [f.relative_to(root_path) for f in (root / "src").glob("**/*.c") if filter_filename(f)]

def find_h_files(root: pathlib.Path):
    return [f.relative_to(root_path) for f in (root / "include").glob("**/*.h")]

c_files = find_c_files(root=root_path)
c_files_string = "\n".join(f"  ${{{project_name}_SOURCE_DIR}}/{pathlib.PurePosixPath(f)}" for f in c_files)

print(f"Includes {len(c_files)} C files.")

h_files = find_h_files(root=root_path)
c_headers_string = "\n".join(f"  ${{{project_name}_SOURCE_DIR}}/{pathlib.PurePosixPath(f)}" for f in h_files)

cmake_contents = f"""
CMAKE_MINIMUM_REQUIRED (VERSION 3.20)
PROJECT (
    {project_name}
    LANGUAGES C
)
INCLUDE (GNUInstallDirs)

SET (CMAKE_C_STANDARD 99)

FIND_PACKAGE (MPI REQUIRED)

FIND_PACKAGE (HDF5 REQUIRED COMPONENTS C)

if(NOT ${{HDF5_IS_PARALLEL}})
    MESSAGE ( FATAL_ERROR "The provided HDF5 must be built with MPI.")
endif()

SET (CMAKE_BUILD_TYPE "Release")

INCLUDE_DIRECTORIES (
    ${{{project_name}_SOURCE_DIR}}/include
    ${{MPI_C_INCLUDE_DIRS}}
    ${{HDF5_INCLUDE_DIRS}}
)

SET (
    PETSC_SOURCES
    {c_files_string}
)

ADD_LIBRARY(
    petsc
    ${{PETSC_SOURCES}}
)

TARGET_LINK_LIBRARIES( petsc )

INSTALL (TARGETS petsc
    LIBRARY DESTINATION ${{CMAKE_INSTALL_LIBDIR}}
)

INSTALL (
    DIRECTORY "${{{project_name}_SOURCE_DIR}}/include/"
    DESTINATION ${{CMAKE_INSTALL_INCLUDEDIR}}
    FILES_MATCHING PATTERN "*.h*"
)
"""

with cmake_file.open('w') as fh:
    fh.write(cmake_contents)