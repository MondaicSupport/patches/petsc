static const char *petscmachineinfo = "\n"
"-----------------------------------------\n"
"Libraries compiled on 2023-09-27 10:47:20 on DESKTOP-JCKBP3N \n"
"Machine characteristics: MINGW64_NT-10.0-22621-3.4.9.x86_64-x86_64-64bit-WindowsPE\n"
"Using PETSc directory: /c/Users/lionk/workspace/Mondaic/Core/build/external/third-party-install/petsc_win\n"
"Using PETSc arch: \n"
"-----------------------------------------\n";
static const char *petsccompilerinfo = "\n"
"Using C compiler: mpicc  -fPIC -Wall -Wwrite-strings -Wno-unknown-pragmas -Wno-lto-type-mismatch -fstack-protector -fvisibility=hidden -g -O  \n"
"-----------------------------------------\n";
static const char *petsccompilerflagsinfo = "\n"
"Using include paths: -I/c/Users/lionk/workspace/Mondaic/Core/build/external/third-party-install/petsc_win/include\n"
"-----------------------------------------\n";
static const char *petsclinkerinfo = "\n"
"Using C linker: mpicc\n"
"Using libraries: -Wl,-rpath,/c/Users/lionk/workspace/Mondaic/Core/build/external/third-party-install/petsc_win/lib -L/c/Users/lionk/workspace/Mondaic/Core/build/external/third-party-install/petsc_win/lib -lpetsc -Wl,-rpath,/c/Users/lionk/workspace/Mondaic/Core/build/external/third-party-install/petsc_win/lib -L/c/Users/lionk/workspace/Mondaic/Core/build/external/third-party-install/petsc_win/lib -lf2clapack -lf2cblas -lptesmumps -lptscotchparmetisv3 -lptscotch -lptscotcherr -lesmumps -lscotch -lscotcherr -lregex -lstdc++ -lgdi32 -luser32 -ladvapi32 -lkernel32 -lquadmath\n"
"-----------------------------------------\n";